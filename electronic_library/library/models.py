from django.db import models


class Author(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=100)
    last_name = models.CharField(verbose_name='Фамилия', max_length=100)
    date_of_birth = models.DateField(verbose_name='Дата рождения', null=True, blank=True)
    date_of_death = models.DateField(verbose_name='Дата смерти', null=True, blank=True)

    def __str__(self):
        """Возвращает имя в порядке first name, last name."""
        return f'{self.first_name} {self.last_name}'

    class Meta:
        ordering = ['last_name']


class Genre(models.Model):
    name = models.CharField(verbose_name='Жанр', max_length=100, help_text='Введите жанр книги')

    def __str__(self):
        """Метод возвращает название жанра."""
        return self.name


class Book(models.Model):
    title = models.CharField(verbose_name='Название книги', max_length=200)
    author = models.ForeignKey(Author, verbose_name='Автор', on_delete=models.CASCADE, null=True)
    description = models.TextField(verbose_name='Подробная информация о книге',
                                   help_text='Введите краткое описание книги')
    genre = models.ManyToManyField(Genre, verbose_name='Жанр', help_text='Выберите жанр для этой книги')

    def display_genre(self):
        return ', '.join(genre.name for genre in self.genre.all()[:3])

    display_genre.short_description = 'Genre'

    def __str__(self):
        return "{} {}".format(self.title, self.author)
