from django.shortcuts import render, get_object_or_404
from library.models import Book, Author, Genre
from django.views.generic import ListView
from django.db.models import Q
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin


@login_required
def index(request):
    num_books = Book.objects.all().count()
    num_authors = Author.objects.all().count()
    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1
    context = {
        'num_books': num_books,
        'num_authors': num_authors,
        'num_visits': num_visits,
    }
    return render(request, 'library/index.html', context=context)


class BookListView(LoginRequiredMixin, ListView):
    model = Book
    paginate_by = 3


@login_required
def book_detail(request, pk):
    book = get_object_or_404(Book, pk=pk)
    return render(request, 'library/book_detail.html', {'book': book})


class SearchResultsView(LoginRequiredMixin, ListView):
    model = Book
    template_name = 'search.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        query_arr = query.split()

        if ' ' in query:
            object_list = Book.objects.filter(Q(author__first_name=query_arr[0]) and Q(author__last_name=query_arr[1])
                                              | Q(title__icontains=query))

        else:
            object_list = Book.objects.filter(Q(author__first_name=query) | Q(author__last_name=query))

        return object_list


@login_required
def edit(request):
    return render(request, 'library/edit.html')


class AddBook(LoginRequiredMixin, CreateView):
    model = Book
    fields = '__all__'
    template_name = 'library/add-book.html'
    success_url = reverse_lazy('add-book')


class ListBook(LoginRequiredMixin, ListView):
    model = Book
    queryset = Book.objects.all()
    context_object_name = 'books'
    template_name = 'library/list-book.html'


class UpdateBook(LoginRequiredMixin, UpdateView):
    model = Book
    fields = '__all__'
    template_name = 'library/add-book.html'
    success_url = reverse_lazy('list-book')


class DeleteBook(LoginRequiredMixin, DeleteView):
    model = Book
    fields = '__all__'
    template_name = 'library/delete-book.html'
    success_url = reverse_lazy('list-book')


class AddAuthor(LoginRequiredMixin, CreateView):
    model = Author
    fields = '__all__'
    template_name = 'library/add-book.html'
    success_url = reverse_lazy('add-author')


class ListAuthor(LoginRequiredMixin, ListView):
    model = Author
    queryset = Author.objects.all()
    context_object_name = 'authors'
    template_name = 'library/list-author.html'


class UpdateAuthor(LoginRequiredMixin, UpdateView):
    model = Author
    fields = '__all__'
    template_name = 'library/add-book.html'
    success_url = reverse_lazy('list-author')


class DeleteAuthor(LoginRequiredMixin, DeleteView):
    model = Author
    fields = '__all__'
    template_name = 'library/delete-author.html'
    success_url = reverse_lazy('list-author')


class AddGenre(LoginRequiredMixin, CreateView):
    model = Genre
    fields = '__all__'
    template_name = 'library/add-book.html'
    success_url = reverse_lazy('list-genre')


class ListGenre(LoginRequiredMixin, ListView):
    model = Genre
    queryset = Genre.objects.all()
    context_object_name = 'genres'
    template_name = 'library/list-genre.html'


class UpdateGenre(LoginRequiredMixin, UpdateView):
    model = Genre
    fields = '__all__'
    template_name = 'library/add-book.html'
    success_url = reverse_lazy('list-genre')


class DeleteGenre(LoginRequiredMixin, DeleteView):
    model = Genre
    fields = '__all__'
    template_name = 'library/delete-genre.html'
    success_url = reverse_lazy('list-genre')
