from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('books/', views.BookListView.as_view(), name='books'),
    path('book/<int:pk>/', views.book_detail, name='book_detail'),
    path('search/', views.SearchResultsView.as_view(), name='search'),
    path('edit/', views.edit, name='edit'),
    path('add-book/', views.AddBook.as_view(), name='add-book'),
    path('list-book/', views.ListBook.as_view(), name='list-book'),
    path('update-book/<int:pk>/', views.UpdateBook.as_view(), name='update-book'),
    path('delete-book/<int:pk>/', views.DeleteBook.as_view(), name='delete-book'),
    path('add-author/', views.AddAuthor.as_view(), name='add-author'),
    path('list-author/', views.ListAuthor.as_view(), name='list-author'),
    path('update-author/<int:pk>/', views.UpdateAuthor.as_view(), name='update-author'),
    path('delete-author/<int:pk>/', views.DeleteAuthor.as_view(), name='delete-author'),
    path('add-genre/', views.AddGenre.as_view(), name='add-genre'),
    path('list-genre/', views.ListGenre.as_view(), name='list-genre'),
    path('update-genre/<int:pk>/', views.UpdateGenre.as_view(), name='update-genre'),
    path('delete-genre/<int:pk>/', views.DeleteGenre.as_view(), name='delete-genre'),
]
