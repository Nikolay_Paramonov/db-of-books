Python 3.7 django  2.2

 

To download the project git clone https://gitlab.com/Nikolay_Paramonov/db-of-books.git. 
To run the project you need to navigate to the folder electronic_library
run the command line and write the python command manage.py runserver.
 
To go to the authorization or registration page in the browser's address bar you need to write http://127.0.0.1:8000/accounts/login/.
After logging in or registering, you will be redirected to the main page.

Clicking on 'All books' opens a page with all the books in the database, page navigation and search for books/authors. 

Clicking on the book title opens a page with detailed information about the book.

 

Clicking on 'edit' opens the page for editing books, authors, and genres

 